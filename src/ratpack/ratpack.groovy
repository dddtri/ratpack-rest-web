import static ratpack.groovy.Groovy.ratpack
import static ratpack.jackson.Jackson.json
//import asset.pipeline.ratpack.AssetPipelineModule
//import asset.pipeline.ratpack.AssetPipelineHandler
//import ratpack.config.ConfigData
import com.service.RunService

ratpack {

	def RunService service = new RunService()

	bindings {
		//ConfigData configData = ConfigData.of().sysProps().build()
		//moduleConfig(new AssetPipelineModule(), configData.get(AssetPipelineModule.Config))
	}

	handlers {
		get {
			//redirect "index.html"

			response.contentType('text/html').send this.getClass().getResource("/public/index.html").text + createTable(service.get())
		}

		post("run/create") {
			def newRow = service.create();
			redirect "/"
		}



		prefix ("rest/run") {
			post("create") {
				def newRow = service.create();
				render(json(newRow))
			}

			delete("delete/:id") {
				def targetRow = service.delete(pathTokens.id)
				render(json(targetRow))
			}
			
			get(":id") {
				//find run
				def targetRow = service.find(pathTokens.id)
				render(json(targetRow))
			}
		}
	}
}

import groovy.xml.MarkupBuilder
def createTable(rows) {
	def writer = new StringWriter()
	new MarkupBuilder(writer).table(style: 'border:1px solid;text-align:center;') {
		tr {
			th('id')
			th('status')
			th('action')
		}
		rows.each { row ->
			tr {
				td(row.id)
				td(row.status)
				td() {
					form("action":"/run/delete/${row.id}", "method":"post"){
						//a("href":"/run/delete/${row.id}", "data-method":"delete"){
						input("type":"submit", "value":"delete")
					}
				}
			}
		}
	}
	writer.toString()
}



