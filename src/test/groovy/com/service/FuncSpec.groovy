package com.service

import com.fasterxml.jackson.databind.ObjectMapper
import ratpack.groovy.test.GroovyRatpackMainApplicationUnderTest
import ratpack.test.ApplicationUnderTest
import ratpack.test.http.TestHttpClient
import spock.lang.Shared
import spock.lang.Specification

import static groovy.json.JsonOutput.toJson

class FuncSpec extends Specification {
  @Shared ApplicationUnderTest aut = new GroovyRatpackMainApplicationUnderTest()
  @Shared ObjectMapper mapper = new ObjectMapper()
  @Delegate TestHttpClient client = aut.httpClient
  
  def "test index.html load"() {
    when:
		get('')

    then:
		response.statusCode == 200
		response.body.text.contains "<p>Click to start a run</p>"
	  
  }
  
  def "test rest/run/create"() {
    when:
		post('rest/run/create')
		println response.statusCode 
		println response.body.text
    
    and:
		response.statusCode == 200
		def row = mapper.readValue(response.body.text, Map)

    then:
	    row['status'] == "done"
		row['id'] == 0
  }
  
  def "test rest/run/"() {
	  given:
	  post('rest/run/create')
	  
	  for (def i = 0 ; i <= 1 ; i ++) {
		  when:
			  get("rest/run/${i}")
		  and:
			  response.statusCode == 200
			  def row = mapper.readValue(response.body.text, Map)
		  then:
			  row['status'] == "done"
			  row['id'] == i
	  }
	}
  
  def "test rest/run/delete"() {
	  for (def i = 0 ; i <= 1 ; i ++) {
		  when:
			  delete("rest/delete/${i}")
		  and:
			  response.statusCode == 200
			  def row = mapper.readValue(response.body.text, Map)
		  then:
			  row['status'] == "done"
			  row['id'] == i
	  	  and:
			  get("rest/run/${i}")
			  response.statusCode == 200
			  response.body.text == "null"
	  }
	}
}