package com.service

class RunService {
	def rows = []

	def create() {
		def idx = 0
		if (rows) {
			idx = rows[-1].id + 1;
		}
		def newRow = [id: idx, status:'done']
		rows.add(newRow)
		return newRow
	}

	def delete(def id) {
		def deletedRow = find(id)
		//delete run
		if (deletedRow != null) {
			rows.remove(deletedRow)
		}
		return deletedRow
	}

	def find(def id) {
		def targetRow = rows.find{row -> String.format("%s", row.id) == id}
		return targetRow
	}
	
	def get() {
		return rows
	}


}