# README #

example for ratpack & gradle & docker integration


### How do I get set up? ###

* Summary of set up
     * JAVA8 is required
     * gradle is required
     
* Configuration
     * if any currently, hardcoded into ratpack.groovy as well as in gradle.build
* Dependencies
     * see dependencies block in gradle.build
* Database configuration
    * none
* How to run tests
    1. check out
    2. cd to root folder in command console
    3. enter "gradlew test --info"
* Development Deployment instructions
    1. check out
    2. cd to root folder in command console
    3. enter "gradlew run -t"
    4. load "http://127.0.0.1:8080" with a browser

* Docker Image Build

    1. Open command window and enter the commands in following steps
    2. cd %rootFolder%
    3. gradlew clean install --info
    4. cd %rootFolder%/build/libs
    5. docker build --tag=ruckus/rest-web ./

* Docker Deployment instruction
    1. download ruckus-container.tar
    2. open up command console
    3. enter docker import rukus-container.tar ruckus/container:new
    5. enter docker run -p 8080:5050 ruckus/container:new java -jar /app/rest-web-all.jar
    6. Load "http://192.168.99.100:8080/" with a browser